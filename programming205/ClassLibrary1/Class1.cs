﻿using System;

namespace ClassVelosiped
{
    public class Class1
    {
        public string Name;
        public string ManufactureCountry;
        public int Width;
        public int Length;
        public int MaxLoad;
        public int Size;
        public bool HasEngine;
        public bool HasTrunk;
        public static void Velosiped()
        {
            Console.WriteLine("Введiть назву велосипеда: ");
            string sName = Console.ReadLine();
            Console.WriteLine("Введiть назву краiни-виробника: ");
            string sManufactureCountry = Console.ReadLine();
            Console.WriteLine("Введiть ширину: ");
            string sWidth = Console.ReadLine();
            Console.WriteLine("Введiть довжину: ");
            string sLength = Console.ReadLine();
            Console.WriteLine("Введiть максимальне навантаження: ");
            string sMaxLoad = Console.ReadLine();

            Console.Write("Шкіряне сідло? (y-так, n-нi): ");

            ConsoleKeyInfo keyHasSeat = Console.ReadKey();
            Console.Write("Металева рама? (y-так, n-нi): ");
            ConsoleKeyInfo keyHasMetalFrame = Console.ReadKey();


            Class1 OurBike = new Class1
            {
                Name = sName,
                ManufactureCountry = sManufactureCountry,
                Width = int.Parse(sWidth),
                Length = int.Parse(sLength),
                MaxLoad = int.Parse(sMaxLoad),
                HasEngine = keyHasSeat.Key == ConsoleKey.Y ? true : false,
                HasTrunk = keyHasMetalFrame.Key == ConsoleKey.Y ? true : false,
                Size = int.Parse(sLength) * int.Parse(sWidth)

            };
            Console.WriteLine();
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("Данi про об`ект: ");
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("Назва: " + OurBike.Name);
            Console.WriteLine("Країна-виробник: " + OurBike.ManufactureCountry);
            Console.WriteLine("Ширина: " + OurBike.Width);
            Console.WriteLine("Довжина: " +
             OurBike.Length.ToString());
            Console.WriteLine("Розмір: " +
             OurBike.Size.ToString());
            Console.WriteLine("Максимальне навантаження: " +
             OurBike.MaxLoad.ToString());
            Console.WriteLine(OurBike.HasEngine ? "Шкіряне" :
             "Не шкіряне");
            Console.WriteLine(OurBike.HasTrunk ? "Металева рама" :
             "Не металева рама");
            Console.WriteLine();

            Console.ReadKey();
        }

    }
}
