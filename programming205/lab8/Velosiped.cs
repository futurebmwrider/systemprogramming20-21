﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab8
{
    public class Velosiped
    {
        public string Name;
        public string ManufactureCountry;
        public int Width;
        public int Length;
        public int MaxLoad;
        public bool HasSeat;
        public bool HasMetalFrame;

    }
}
