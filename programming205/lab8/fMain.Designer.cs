﻿namespace lab8
{
    partial class fMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbVelosipedsInfo = new System.Windows.Forms.TextBox();
            this.btnAddVelosiped = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbVelosipedsInfo
            // 
            this.tbVelosipedsInfo.Location = new System.Drawing.Point(12, 12);
            this.tbVelosipedsInfo.Multiline = true;
            this.tbVelosipedsInfo.Name = "tbVelosipedsInfo";
            this.tbVelosipedsInfo.ReadOnly = true;
            this.tbVelosipedsInfo.Size = new System.Drawing.Size(411, 262);
            this.tbVelosipedsInfo.TabIndex = 0;
            // 
            // btnAddVelosiped
            // 
            this.btnAddVelosiped.Location = new System.Drawing.Point(455, 12);
            this.btnAddVelosiped.Name = "btnAddVelosiped";
            this.btnAddVelosiped.Size = new System.Drawing.Size(123, 23);
            this.btnAddVelosiped.TabIndex = 1;
            this.btnAddVelosiped.Text = "Додати велосипед";
            this.btnAddVelosiped.UseVisualStyleBackColor = true;
            this.btnAddVelosiped.Click += new System.EventHandler(this.btnAddVelosiped_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(482, 251);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Закрити";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 286);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnAddVelosiped);
            this.Controls.Add(this.tbVelosipedsInfo);
            this.MaximizeBox = false;
            this.Name = "fMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Лабораторна робота №8";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbVelosipedsInfo;
        private System.Windows.Forms.Button btnAddVelosiped;
        private System.Windows.Forms.Button btnClose;
    }
}

