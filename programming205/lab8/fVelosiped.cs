﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab8
{
    public partial class fVelosiped : Form
    {
        public Velosiped TheVelosiped;
        public fVelosiped(Velosiped v)
        {
            TheVelosiped = v;

            InitializeComponent();
        }

        private void fVelosiped_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyValue == (char)Keys.Enter)
            {
                btnOk_Click(btnOk, null);
            }

            if(e.KeyValue == (char)Keys.Escape)
            {
                btnCancel_Click(btnCancel, null);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            TheVelosiped.Name = tbName.Text.Trim();
            TheVelosiped.ManufactureCountry = tbCountry.Text.Trim();
            TheVelosiped.Width = int.Parse(tbWidth.Text.Trim());
            TheVelosiped.Length = int.Parse(tbLength.Text.Trim());
            TheVelosiped.MaxLoad = int.Parse(tbMaxLoad.Text.Trim());
            TheVelosiped.HasSeat = chbHasSeat.Checked;
            TheVelosiped.HasMetalFrame = chbHasMetalFrame.Checked;

            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void fVelosiped_Load(object sender, EventArgs e)
        {
            if(TheVelosiped != null)
            {
                tbName.Text = TheVelosiped.Name;
                tbCountry.Text = TheVelosiped.ManufactureCountry;
                tbWidth.Text = TheVelosiped.Width.ToString();
                tbLength.Text = TheVelosiped.Length.ToString();
                tbMaxLoad.Text = TheVelosiped.MaxLoad.ToString();
                chbHasSeat.Checked = TheVelosiped.HasSeat;
                chbHasMetalFrame.Checked = TheVelosiped.HasMetalFrame;
            }
        }
    }
}
