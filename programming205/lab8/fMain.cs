﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab8
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        private void btnAddVelosiped_Click(object sender, EventArgs e)
        {
            Velosiped velosiped = new Velosiped();

            fVelosiped fv = new fVelosiped(velosiped);

            if (fv.ShowDialog() == DialogResult.OK)
            {
                tbVelosipedsInfo.Text += string.Format("{0},\r\n{1},\r\n{2}мм\r\n{3}мм\r\n{4}кг\r\n{5}\r\n\r\n", velosiped.Name, velosiped.ManufactureCountry, velosiped.Width, velosiped.Length, velosiped.MaxLoad, velosiped.HasSeat ? "Шкіряне" : "Не шкіряне", velosiped.HasMetalFrame ? "Металева" : "Не металева");
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Припинити роботу застосунку?",
"Припинити роботу", MessageBoxButtons.OKCancel,
MessageBoxIcon.Question) == DialogResult.OK)
                Application.Exit();
        }
    }
}
